import PropTypes from "prop-types";
import "./button.scss"

export default function Button({onClick, code, text}){
    return <button className="btn" onClick={onClick} data-id={code}>{text}</button>
}
Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    code: PropTypes.string,
}
Button.defaultProps = {
    text: "Click me!",
    onClick: ()=>{
        console.log("Clicked");
    },
    code: "product code",
}