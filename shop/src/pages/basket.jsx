import CardList from "../components/CardList";
import { useSelector } from "react-redux";


export function Basket (){
    const products = useSelector((state) => state.products.products);
    const basket = useSelector((state) => state.cart.basket);
    if (basket.length > 0) {
        let arr = [];
        products.forEach(element => {
            if(basket.includes(element.barcode)){
                arr.push(element);
            }
        });
        return (
            <>
            <CardList
                    arr={arr}
                    delBtn={true}
                />
            </>
        )
    }
    return <h1 className="no_data">No products in your basket</h1>
}